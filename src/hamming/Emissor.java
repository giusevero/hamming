/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hamming;

/**
 *
 * @author giusevero
 */
public class Emissor {

    private String msg; //Mensagem em binário
    private char[] msgArray; //Mensagem binário dentro de um vetor
    private int R; //Qtnd de bits de redundância

    public Emissor(String msg) {
        this.msg = msg;
        msgArray = this.msg.toCharArray();
    }
    
    
    public void calculaR(){
        int tam = msgArray.length;
        
        int r=0;
        int result = 1;
        double result1 = 0;
        
        
        while(result1<result){
            result = tam + r + 1;
            result1 = Math.pow(2, r);
            if (result1 >= result) {
                this.R = r;
            }
            r++;
        }
        
        transformaEmBit(R);
    }

    private int[] transformaEmBit(int R){
        
        int[] array= new int[R];
        
        for(int i= 0;i<array.length;i++){
            array[i] = i;
        }
        
        return array;
    }
    
    
    public int getR() {
        return R;
    }

    public void setR(int R) {
        this.R = R;
    }
    
    
}
